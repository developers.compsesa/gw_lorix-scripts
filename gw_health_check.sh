#!/bin/bash
#HEALTH CHECK USED FOR LORIX ONE GATEWAYS
#YOU HAVE TO ADD THIS SCRIPT TO THE CRONTAB TO REPORT EVERY X MINUTES/HOURS/DAYS
#YOU'LL ALSO NEED TO CHANGE THE IP AND PORT OF THE INFLUX DATABASE AND PROVIDE A USER W/ PASSWORD

EUI=$(cat /sys/class/net/eth0/address)
SERIAL=$(cat /sys/class/net/eth0/address)
INET=$(/sbin/ifconfig | grep -Eom 1 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
PNET=$(wget -qO- https://ifconfig.co/ip)

if [[ $? -eq 0 ]]; then
  wget --post-data "health,eui=${EUI//:},serial=${SERIAL},inet=${INET},pnet=${PNET} value=1" \
  "http://192.168.1.42:8086/write?db=gateways"

  if [[ $? -eq 0 ]]; then
    echo "$(date -u) Health check succeeded."
  else
    echo "$(date -u) Health check failed."
  fi
fi



